package com.duxgames.whichmovietosee.dialogs;

import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import com.duxgames.whichmovietosee.R;
import com.duxgames.whichmovietosee.base.Film;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class FingFilmDialog extends AlertDialog {
	
	ListView listView;
	
	public FingFilmDialog(final Context context, Film film) {
		super(context);

		View view = (RelativeLayout) ((Activity) context).getLayoutInflater().inflate(R.layout.dialog_find_film, null);

		ArrayList<Link> links = new ArrayList<FingFilmDialog.Link>();
		try {
			links.add(new Link(context.getString(R.string.find_google_play), "https://play.google.com/store/search?q="+URLEncoder.encode(film.namerus, "UTF-8")+"&c=movies"));
			links.add(new Link(context.getString(R.string.find_vk), "http://m.vk.com/video?len=2&q="+URLEncoder.encode(film.namerus, "UTF-8")+"&section=search"));
		} catch(Exception e) {
			dismiss();
			e.printStackTrace();
			return;
		}
		final FindFilmAdapter adapter = new FindFilmAdapter(getContext(), R.layout.item_find_film, links);
		listView = (ListView) view.findViewById(R.id.listView);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				try {
					URL url = new URL(adapter.getItem(position).link);
					Intent i = new Intent(Intent.ACTION_VIEW);
					i.setData(Uri.parse(url.toString()));
					context.startActivity(i);
				} catch (Exception e) {
					Toast.makeText(context, "ошибка", Toast.LENGTH_SHORT).show();
				}
			}
			
		});
		setView(view);

		setCancelable(true);
	}

	class Link {
		public String name;
		public String link;
		
		public Link(String name,String link) {
			this.name = name;
			this.link = link;
		}
	}
	
	public class FindFilmAdapter extends ArrayAdapter<Link> {

		public FindFilmAdapter(Context context, int resource, List<Link> links) {
			super(context, resource, links);
		}

		@Override
		public View getView(final int position, View view, ViewGroup parent) {
			view = LayoutInflater.from(getContext()).inflate(R.layout.item_grid, parent, false);
			TextView name = (TextView) view.findViewById(R.id.textViewName);
			name.setText(getItem(position).name);
			return view;
		}

	}

	
	
}