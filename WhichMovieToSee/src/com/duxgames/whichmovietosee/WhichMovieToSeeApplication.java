package com.duxgames.whichmovietosee;

import org.acra.ACRA;
import org.acra.ErrorReporter;
import org.acra.annotation.ReportsCrashes;

import android.app.Application;

@ReportsCrashes(formKey = "",
	formUri = "http://duxcast.com/reporter.php")

public class WhichMovieToSeeApplication extends Application {

	@Override
	public void onCreate() {
		ACRA.init(this); 
        ErrorReporter.getInstance().checkReportsOnApplicationStart();
		super.onCreate();
	}

}
