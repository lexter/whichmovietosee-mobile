package com.duxgames.whichmovietosee.activites;

import org.json.JSONArray;

import com.duxgames.whichmovietosee.R;
import com.duxgames.whichmovietosee.base.Film;
import com.duxgames.whichmovietosee.db.DbOpenHelper;
import com.duxgames.whichmovietosee.dialogs.FingFilmDialog;
import com.duxgames.whichmovietosee.utils.BitmapHelper;
import com.duxgames.whichmovietosee.utils.DrawUtils;
import com.duxgames.whichmovietosee.utils.HtmlHelper;
import com.duxgames.whichmovietosee.utils.HttpCommand;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {

	Handler handler;
	Film film;
	TextView textViewName;
	TextView textViewNameEng;
	TextView textViewDesc;
	TextView textViewDesc2;
	TextView textViewRatingKino;
	TextView textViewRatingImdb;
	ImageView label;
	ImageButton buttonNext;
	ImageButton buttonKinogo,buttonBookmark;
	ProgressDialog progress;
	RelativeLayout mainLayout;
	View includeWelcome,includeFilm;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		handler = new Handler();
		progress = new ProgressDialog(this);
		textViewName = (TextView) findViewById(R.id.textViewName);
		textViewNameEng = (TextView) findViewById(R.id.textViewNameEng);
		textViewDesc = (TextView) findViewById(R.id.textViewDesc);
		textViewDesc2 = (TextView) findViewById(R.id.textViewDesc2);
		textViewRatingKino = (TextView) findViewById(R.id.textViewRatingKino);
		textViewRatingImdb = (TextView) findViewById(R.id.textViewRatingImdb);
		label = (ImageView) findViewById(R.id.imageViewLabel);
		buttonNext = (ImageButton) findViewById(R.id.buttonNext);
		mainLayout = (RelativeLayout) findViewById(R.id.mainLayout);
		buttonKinogo = (ImageButton) findViewById(R.id.buttonFindKino);
		buttonBookmark = (ImageButton) findViewById(R.id.buttonBookmark);
		
		includeWelcome =  findViewById(R.id.frame_welcome);
		includeWelcome.setVisibility(View.VISIBLE);
		includeFilm =  findViewById(R.id.frame_film);
		includeFilm.setVisibility(View.GONE);
		
		buttonBookmark.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				film.inBookmark = !film.inBookmark;
				if (film.inBookmark) {
					film.save(DbOpenHelper.getInstance(getApplicationContext()).getDb());
				} else {
					film.remove(DbOpenHelper.getInstance(getApplicationContext()).getDb());
				}
				updateBookmark();
			}
		});
		
		buttonNext.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				nextFilm();
			}
		});
		

		
		
		final AdView adView = (AdView)this.findViewById(R.id.adView);
	    AdRequest adRequest = new AdRequest.Builder().build();
	    adView.loadAd(adRequest);
	    adView.setAdListener(new AdListener() {
	    	  public void onAdLoaded() {
	    		  adView.setVisibility(View.VISIBLE);
	    	  }
	    	  public void onAdFailedToLoad(int errorcode) {
	    		  adView.setVisibility(View.GONE);
	    	  }
	    });
	    
	}
	
	@Override
	protected void onResume() {
		if (film!=null)
			if (buttonBookmark!=null) {
				film.updateCurrentBookmark(DbOpenHelper.getInstance(getApplicationContext()).getDb());
				updateBookmark();
			} 
		super.onResume();
	}
	
	void nextFilm() {
		showDialog();
		new Thread(new UpdateCache(getApplicationContext())).start();
	}

	void updateBookmark() {
		if (film.inBookmark)
			buttonBookmark.setImageResource(R.drawable.ic_like);
		else
			buttonBookmark.setImageResource(R.drawable.ic_unlike);
	}
	
	void updateViews() {
		if (film==null) {
			dismissDialog();
			return;
		}
		int colorTitle = DrawUtils.averangeColor(film.label);
		label.setImageBitmap(film.label);
		
		includeWelcome.setVisibility(View.GONE);
		includeFilm.setVisibility(View.VISIBLE);
		
		textViewName.setText(film.namerus);
		textViewName.setShadowLayer(3, 0, 0, DrawUtils.doubleRevertColor(colorTitle));
		textViewName.setTextColor(DrawUtils.revertColor(colorTitle));
		
		textViewNameEng.setText(film.nameeng);
		textViewNameEng.setShadowLayer(3, 0, 0, DrawUtils.doubleRevertColor(colorTitle));
		textViewNameEng.setTextColor(DrawUtils.revertColor(colorTitle));
		
		textViewDesc.setText(film.description);
		textViewDesc2.setText(film.genre);
		
		textViewRatingKino.setText(film.kinopoisk);
		textViewRatingImdb.setText(film.imdb);
		
		mainLayout.setBackgroundDrawable(new BitmapDrawable(film.labelback));
		
		buttonKinogo.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				new FingFilmDialog(MainActivity.this, film).show();
			}
		});
		
		updateBookmark(); 
		dismissDialog();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		int id = item.getItemId();
		if (id == R.id.action_settings) {
			Intent intent = new Intent(this,SettingsActivity.class);
			startActivity(intent);
			return true;
		}
		if (id == R.id.action_favorite) {
			Intent intent = new Intent(this,BookmarksActivity.class);
			startActivity(intent);
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	public class UpdateCache implements Runnable {
		
		Context context;
		
		public UpdateCache(Context context) {
			this.context = context;
		}

		@Override
		public void run() {
			try {
				if (handler==null) handler = new Handler(getMainLooper());
				
				HttpCommand command = new HttpCommand(context);
				String comandResult = command.executeForString();
				JSONArray object = new JSONArray(comandResult);
				if (object.length()==0) {
					handler.post(new Runnable() {
						@Override
						public void run() {
							dismissDialog();
							Toast.makeText(context, "По запросу ничего не найдено, измените параметры поиска.", Toast.LENGTH_SHORT).show();
						}
					});
				}
				for (int i=0;i<object.length();i++) {
					HtmlHelper helper = new HtmlHelper(object.get(i).toString());
					film = new Film();
					film.label = BitmapHelper.getPicture(context, helper.getPhotoLink());
					film.labelback = DrawUtils.fastblur(film.label, 20);
					film.namerus = helper.getNameRus();
					film.nameeng = helper.getNameEng();
					film.description = helper.getDescription();
					film.genre = helper.getGenre();
					film.kinopoisk = helper.getRatingKino();
					film.imdb = helper.getRatingImgb();
				}
				
				handler.post(new Runnable() {
					@Override
					public void run() {
						updateViews();
					}
				});
			} catch (Exception e) {
				e.printStackTrace();
				if (handler==null) handler = new Handler(getMainLooper());
				handler.post(new Runnable() {
					@Override
					public void run() {
						dismissDialog();
						Toast.makeText(context, "Отсутствует соединение", Toast.LENGTH_SHORT).show();
					}
				});
			}
			
		}
	}
	
	void showDialog() {
		if (progress!=null) {
			progress.setTitle("Обновление");
	        progress.setMessage("Загрузка...");
	        progress.setIndeterminate(false);
	        progress.setCanceledOnTouchOutside(false);
	        progress.show();
		}
	}
	
	void dismissDialog() {
		if (progress!=null) progress.dismiss();
	}


}
