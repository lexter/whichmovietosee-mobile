package com.duxgames.whichmovietosee.activites;

import java.io.ByteArrayOutputStream;

import com.duxgames.whichmovietosee.R;
import com.duxgames.whichmovietosee.adapters.BookmarksAdapter;
import com.duxgames.whichmovietosee.base.Film;
import com.duxgames.whichmovietosee.db.DbOpenHelper;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class BookmarksActivity extends ActionBarActivity {

	Handler handler;
	ListView listView;
	BookmarksAdapter adapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bookmarks_list);
		handler = new Handler();

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
		adapter = new BookmarksAdapter(getApplicationContext(), R.layout.item_like, Film.getBookmarkFilms(getApplicationContext(), DbOpenHelper.getInstance(getApplicationContext()).getDb()));
		listView = (ListView) findViewById(R.id.listLike);
		listView.setAdapter(adapter);

		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
				Intent intent = new Intent(BookmarksActivity.this, SeeFilmActivity.class);
				intent.putExtra("film_id", adapter.getItem(position)._id);
				intent.putExtra("film_namerus", adapter.getItem(position).namerus);
				intent.putExtra("film_nameeng", adapter.getItem(position).nameeng);
				intent.putExtra("film_description", adapter.getItem(position).description);
				intent.putExtra("film_genre", adapter.getItem(position).genre);
				intent.putExtra("film_imdb", adapter.getItem(position).imdb);
				intent.putExtra("film_kinopoisk", adapter.getItem(position).kinopoisk);
				
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				adapter.getItem(position).label.compress(Bitmap.CompressFormat.PNG, 100, bos);
				byte[] blob = bos.toByteArray();
				intent.putExtra("film_label", blob);
				
				ByteArrayOutputStream bos2 = new ByteArrayOutputStream();
				adapter.getItem(position).labelback.compress(Bitmap.CompressFormat.PNG, 100, bos2);
				byte[] blob2 = bos2.toByteArray();
				intent.putExtra("film_labelback", blob2);
				
				startActivity(intent);
			}
			
		});
	}
	
	@Override
	protected void onResume() {
		if (adapter!=null) {
			adapter.clear();
			adapter.addAll(Film.getBookmarkFilms(getApplicationContext(), DbOpenHelper.getInstance(getApplicationContext()).getDb()));
			adapter.notifyDataSetChanged();
		}
		super.onResume();
	}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.bookmark, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		int id = item.getItemId();
		if (id == android.R.id.home) {
			finish();
			return true;
		}
		
		return super.onOptionsItemSelected(item);
	}

}
