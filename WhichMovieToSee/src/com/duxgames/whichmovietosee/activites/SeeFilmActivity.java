package com.duxgames.whichmovietosee.activites;

import com.duxgames.whichmovietosee.R;
import com.duxgames.whichmovietosee.base.Film;
import com.duxgames.whichmovietosee.db.DbOpenHelper;
import com.duxgames.whichmovietosee.dialogs.FingFilmDialog;
import com.duxgames.whichmovietosee.utils.DrawUtils;

import android.app.ProgressDialog;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
public class SeeFilmActivity extends ActionBarActivity {

	Handler handler;
	Film film;
	TextView textViewName;
	TextView textViewNameEng;
	TextView textViewDesc;
	TextView textViewDesc2;
	TextView textViewRatingKino;
	TextView textViewRatingImdb;
	ImageView label;
	Button buttonNext;
	ImageButton buttonKinogo,buttonBookmark;
	ProgressDialog progress;
	RelativeLayout mainLayout;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_see);
		handler = new Handler();
		
		try {
			film = new Film();
			film._id = getIntent().getExtras().getInt("film_id");
			film.namerus = getIntent().getExtras().getString("film_namerus");
			film.nameeng = getIntent().getExtras().getString("film_nameeng");
			film.description = getIntent().getExtras().getString("film_description");
			film.genre = getIntent().getExtras().getString("film_genre");
			film.imdb = getIntent().getExtras().getString("film_imdb");
			film.kinopoisk = getIntent().getExtras().getString("film_kinopoisk");
			film.label = BitmapFactory.decodeByteArray(getIntent().getExtras().getByteArray("film_label"), 0, getIntent().getExtras().getByteArray("film_label").length);
			film.labelback = BitmapFactory.decodeByteArray(getIntent().getExtras().getByteArray("film_labelback"), 0, getIntent().getExtras().getByteArray("film_labelback").length);
		} catch(Exception e) {
			e.printStackTrace();
			finish();
			return;
		}
		
		if (film==null) {
			finish();
			return;
		}
		
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
		progress = new ProgressDialog(this);
		textViewName = (TextView) findViewById(R.id.textViewName);
		textViewNameEng = (TextView) findViewById(R.id.textViewNameEng);
		textViewDesc = (TextView) findViewById(R.id.textViewDesc);
		textViewDesc2 = (TextView) findViewById(R.id.textViewDesc2);
		textViewRatingKino = (TextView) findViewById(R.id.textViewRatingKino);
		textViewRatingImdb = (TextView) findViewById(R.id.textViewRatingImdb);
		label = (ImageView) findViewById(R.id.imageViewLabel);
		buttonNext = (Button) findViewById(R.id.buttonNext);
		mainLayout = (RelativeLayout) findViewById(R.id.mainLayout);
		buttonKinogo = (ImageButton) findViewById(R.id.buttonFindKino);
		buttonBookmark = (ImageButton) findViewById(R.id.buttonBookmark);
		
		buttonBookmark.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				film.inBookmark = !film.inBookmark;
				if (film.inBookmark) {
					film.save(DbOpenHelper.getInstance(getApplicationContext()).getDb());
				} else {
					film.remove(DbOpenHelper.getInstance(getApplicationContext()).getDb());
				}
				updateBookmark();
			}
		});
		
		updateViews();
	}
	
	@Override
	protected void onResume() {
		if (film!=null)
			if (buttonBookmark!=null) {
				film.updateCurrentBookmark(DbOpenHelper.getInstance(getApplicationContext()).getDb());
				updateBookmark();
			}
		super.onResume();
	}
	
	void updateBookmark() {
		if (film.inBookmark)
			buttonBookmark.setImageResource(R.drawable.ic_like);
		else
			buttonBookmark.setImageResource(R.drawable.ic_unlike);
	}
	
	void updateViews() {
		int colorTitle = DrawUtils.averangeColor(film.label);
		label.setImageBitmap(film.label);
		
		textViewName.setText(film.namerus);
		textViewName.setShadowLayer(3, 0, 0, DrawUtils.doubleRevertColor(colorTitle));
		textViewName.setTextColor(DrawUtils.revertColor(colorTitle));
		
		textViewNameEng.setText(film.nameeng);
		textViewNameEng.setShadowLayer(3, 0, 0, DrawUtils.doubleRevertColor(colorTitle));
		textViewNameEng.setTextColor(DrawUtils.revertColor(colorTitle));
		
		textViewDesc.setText(film.description);
		textViewDesc2.setText(film.genre);
		
		textViewRatingKino.setText(film.kinopoisk);
		textViewRatingImdb.setText(film.imdb);
		
		mainLayout.setBackgroundDrawable(new BitmapDrawable(film.labelback));
		
		buttonKinogo.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				new FingFilmDialog(SeeFilmActivity.this, film).show();
			}
		});
		
		updateBookmark(); 
		dismissDialog();
	}
	
	void showDialog() {
		if (progress!=null) {
			progress.setTitle("Обновление");
	        progress.setMessage("Загрузка...");
	        progress.setIndeterminate(false);
	        progress.setCanceledOnTouchOutside(false);
	        progress.show();
		}
	}
	
	void dismissDialog() {
		if (progress!=null) progress.dismiss();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.see, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		int id = item.getItemId();
		if (id == android.R.id.home) {
			finish();
			return true;
		}
		
		return super.onOptionsItemSelected(item);
	}

}
