package com.duxgames.whichmovietosee.activites;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.duxgames.whichmovietosee.R;
import com.duxgames.whichmovietosee.R.id;
import com.duxgames.whichmovietosee.R.layout;
import com.duxgames.whichmovietosee.R.menu;
import com.duxgames.whichmovietosee.adapters.GridAdapter;
import com.duxgames.whichmovietosee.base.Choisen;
import com.duxgames.whichmovietosee.base.Country;
import com.duxgames.whichmovietosee.base.Genre;
import com.duxgames.whichmovietosee.utils.ExpandableHeightGridView;
import com.duxgames.whichmovietosee.utils.RangeSeekBar;
import com.duxgames.whichmovietosee.utils.RangeSeekBar.OnRangeSeekBarChangeListener;
import com.duxgames.whichmovietosee.utils.Settings;


import android.os.Bundle;
import android.app.Activity;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SettingsActivity extends ActionBarActivity {

	Date fromDate;
	Date toDate;
	
	TextView tvFromDate;
	TextView tvtoDate;

	GridAdapter genresAdapter;
	GridAdapter countrysAdapter;
	
	ArrayList<Integer> genresSelected = new ArrayList<Integer>();
	ArrayList<Integer> countrysSelected = new ArrayList<Integer>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);
		
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
		loadSettings();

		try {
			
			tvFromDate = (TextView) findViewById(R.id.fromDate);
			tvtoDate = (TextView) findViewById(R.id.toDate);
			LinearLayout layout = (LinearLayout) findViewById(R.id.layoutForSeek);
			
			Date minDate = new SimpleDateFormat("yyyy-MM-dd").parse("1960-01-01");
			Date maxDate = new SimpleDateFormat("yyyy-MM-dd").parse("2016-01-01");
			
			if (fromDate==null) fromDate = minDate;
			if (toDate==null) toDate = maxDate;
			tvFromDate.setText(String.format("%tY", fromDate));
        	tvtoDate.setText(String.format("%tY", toDate));
			
			
			RangeSeekBar<Long> seekBar = new RangeSeekBar<Long>(minDate.getTime(), maxDate.getTime(), getApplicationContext());
			seekBar.setSelectedMinValue(fromDate.getTime());
			seekBar.setSelectedMaxValue(toDate.getTime());
			seekBar.setOnRangeSeekBarChangeListener(new OnRangeSeekBarChangeListener<Long>() {
			        @Override
			        public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar, Long minValue, Long maxValue) {
			        	fromDate = new Date(minValue);
			        	toDate = new Date(maxValue);
			        	tvFromDate.setText(String.format("%tY", fromDate));
			        	tvtoDate.setText(String.format("%tY", toDate));
			        }
			});
	
			layout.addView(seekBar);
			
			//-=-=-=-=-=-=-=-=-=-=-=-=-
			
			ExpandableHeightGridView gridViewGenres = (ExpandableHeightGridView) findViewById(R.id.gridViewGenres);
			gridViewGenres.setExpanded(true);
			ArrayList<Choisen> genres = new ArrayList<Choisen>();
			genres.addAll(Genre.getAllGenres());
			genresAdapter = new GridAdapter(getApplicationContext(), R.layout.item_grid, genres, genresSelected);
			gridViewGenres.setAdapter(genresAdapter);
			gridViewGenres.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					clickGenreItem(genresAdapter.getItem(position)._id);
					genresAdapter.notifyDataSetChanged();
				}
				
			});
			
			gridViewGenres.setOnTouchListener(new OnTouchListener(){
			    @Override
			    public boolean onTouch(View v, MotionEvent event) {
			        if(event.getAction() == MotionEvent.ACTION_MOVE){
			            return true;
			        }
			        return false;
			    }

			});
			
			//-=-=-=-=-=-=-=-=-=-=-=-=-
			
			ExpandableHeightGridView gridViewCountrys = (ExpandableHeightGridView) findViewById(R.id.gridViewCountrys);
			gridViewCountrys.setExpanded(true);
			ArrayList<Choisen> countrys = new ArrayList<Choisen>();
			countrys.addAll(Country.getAllCountry());
			countrysAdapter = new GridAdapter(getApplicationContext(), R.layout.item_grid, countrys, countrysSelected);
			gridViewCountrys.setAdapter(countrysAdapter);
			gridViewCountrys.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					clickCountryItem(countrysAdapter.getItem(position)._id);
					countrysAdapter.notifyDataSetChanged();
				}
				
			});
			gridViewCountrys.setOnTouchListener(new OnTouchListener(){
			    @Override
			    public boolean onTouch(View v, MotionEvent event) {
			        if(event.getAction() == MotionEvent.ACTION_MOVE){
			            return true;
			        }
			        return false;
			    }

			});
			
		} catch(Exception e) {
			e.printStackTrace();
			finish();
		}
		
	}

	void clickGenreItem(int _id) {
		for (Integer select_id:genresSelected) {
			if (select_id==_id) {
				genresSelected.remove(select_id);
				return;
			}
		}
		genresSelected.add(_id);
	}
	
	void clickCountryItem(int _id) {
		for (Integer select_id:countrysSelected) {
			if (select_id==_id) {
				countrysSelected.remove(select_id);
				return;
			}
		}
		countrysSelected.add(_id);
	}
	
	void saveSettings() {

		try {
			JSONObject time = new JSONObject();
			time.put("fromDate", fromDate.getTime());
			time.put("toDate", toDate.getTime());
			Settings.setParam("time", time.toString(), getApplicationContext());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		JSONArray genres = new JSONArray();
		for (Integer id:genresSelected) {
			genres.put(id);
		}
		Settings.setParam("genres", genres.toString(), getApplicationContext());
		
		JSONArray countrys = new JSONArray();
		for (Integer id:countrysSelected) {
			countrys.put(id);
		}
		Settings.setParam("countrys", countrys.toString(), getApplicationContext());
	}
	
	void loadSettings() {
		try {
			JSONObject time = new JSONObject(Settings.getParam("time", getApplicationContext()));
			fromDate = new Date(time.getLong("fromDate"));
			toDate = new Date(time.getLong("toDate"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			JSONArray genres = new JSONArray(Settings.getParam("genres", getApplicationContext()));
			for (int i=0;i<genres.length();i++) {
				genresSelected.add(genres.getInt(i));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
			
		try {	
			JSONArray countrys = new JSONArray(Settings.getParam("countrys", getApplicationContext()));
			for (int i=0;i<countrys.length();i++) {
				countrysSelected.add(countrys.getInt(i));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.settings, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		int id = item.getItemId();
		if (id == R.id.action_apply) {
			saveSettings();
			finish();
			return true;
		}
		if (id == android.R.id.home) {
			finish();
			return true;
		}
		
		return super.onOptionsItemSelected(item);
	}

}
