package com.duxgames.whichmovietosee.adapters;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.duxgames.whichmovietosee.R;
import com.duxgames.whichmovietosee.base.Choisen;

public class GridAdapter extends ArrayAdapter<Choisen> {

	ArrayList<Integer> selected;

	public GridAdapter(Context context, int resource, List<Choisen> genres,
			ArrayList<Integer> selected) {
		super(context, resource, genres);
		this.selected = selected;
	}

	@Override
	public View getView(final int position, View view, ViewGroup parent) {
		view = LayoutInflater.from(getContext()).inflate(R.layout.item_grid,
				parent, false);
		TextView name = (TextView) view.findViewById(R.id.textViewName);
		name.setText(getItem(position).name);
		for (Integer id : selected) {
			if (id == getItem(position)._id) {
				view.setBackgroundResource(R.color.bg_blue);
			}
		}
		return view;
	}

}
