package com.duxgames.whichmovietosee.adapters;

import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.duxgames.whichmovietosee.R;
import com.duxgames.whichmovietosee.base.Film;
import com.duxgames.whichmovietosee.db.DbOpenHelper;
import com.duxgames.whichmovietosee.utils.DrawUtils;

public class BookmarksAdapter extends ArrayAdapter<Film> {

		public BookmarksAdapter(Context context, int resource, List<Film> films) {
			super(context, resource, films);
		}
		
		@Override
		public View getView(final int position, View view, ViewGroup parent) {
			if (view!=null) {
				if (view.findViewById(R.id.textViewName).getTag().equals(getItem(position).namerus)) return view;
			}
			
			view = LayoutInflater.from(getContext()).inflate(R.layout.item_like, parent, false);
			final TextView textViewName = (TextView) view.findViewById(R.id.textViewName);
			final ImageView imageView = (ImageView) view.findViewById(R.id.imageViewBkg);
			textViewName.setText(getItem(position).namerus);
			textViewName.setTag(getItem(position).namerus);
			
			final ImageButton bookmarkButton = (ImageButton) view.findViewById(R.id.imageButtonBookmark);
			bookmarkButton.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					getItem(position).inBookmark = !getItem(position).inBookmark;
					if (getItem(position).inBookmark) {
						getItem(position).save(DbOpenHelper.getInstance(getContext()).getDb());
						bookmarkButton.setImageResource(R.drawable.ic_like);
					} else {
						getItem(position).remove(DbOpenHelper.getInstance(getContext()).getDb());
						bookmarkButton.setImageResource(R.drawable.ic_unlike);
					}

				}
			});
			
			new Thread(new Runnable() {
				
				@Override
				public void run() {
					final int colorTitle = DrawUtils.averangeColor(getItem(position).label);
					final Bitmap bitmap = DrawUtils.fastblur(getItem(position).label, 20);
					
					textViewName.post(new Runnable() {
						
						@Override
						public void run() {
							textViewName.setShadowLayer(3, 0, 0, DrawUtils.doubleRevertColor(colorTitle));
							textViewName.setTextColor(DrawUtils.revertColor(colorTitle));

							imageView.setImageBitmap(bitmap);
						}
					});
					
				}
			}).start();
			
			
			return view;
		}
		
	}
