package com.duxgames.whichmovietosee.base;

import java.util.ArrayList;

public class Genre extends Choisen {

	public Genre(int _id,String name) {
		this._id = _id;
		this.name = name;
	}
	
	public static ArrayList<Genre> getAllGenres() {
		ArrayList<Genre> genres = new ArrayList<Genre>();
		genres.add(new Genre(1750, "Анимэ"));
		genres.add(new Genre(22, "Биография"));
		genres.add(new Genre(3, "Боевик"));
		genres.add(new Genre(13, "Вестерн"));
		genres.add(new Genre(19, "Военный"));
		genres.add(new Genre(17, "Детектив"));
		genres.add(new Genre(456, "Детский"));
		genres.add(new Genre(12, "Док-ный"));
		genres.add(new Genre(8, "Драма"));
		genres.add(new Genre(23, "История"));
		genres.add(new Genre(6, "Комедия"));
		genres.add(new Genre(15, "Короткие"));
		genres.add(new Genre(16, "Криминал"));
		genres.add(new Genre(7, "Мелодрама"));
		genres.add(new Genre(21, "Музыка"));
		genres.add(new Genre(14, "Мультик"));
		genres.add(new Genre(9, "Мюзикл"));
		genres.add(new Genre(10, "Приключения"));
		genres.add(new Genre(11, "Семейный"));
		genres.add(new Genre(24, "Спорт"));
		genres.add(new Genre(4, "Триллер"));
		genres.add(new Genre(1, "Ужасы"));
		genres.add(new Genre(2, "Фантастика"));
		//genres.add(new Genre(18, "Фильм-нуар"));
		genres.add(new Genre(5, "Фэнтези"));
		return genres;
	}
	

	
}
