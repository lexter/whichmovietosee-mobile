package com.duxgames.whichmovietosee.base;

import java.util.ArrayList;

public class Country extends Choisen {

	public Country(int _id,String name) {
		this._id = _id;
		this.name = name;
	}
	
	public static ArrayList<Country> getAllCountry() {
		ArrayList<Country> countrys = new ArrayList<Country>();
		countrys.add(new Country(2, "Россия"));
		countrys.add(new Country(1, "США"));
		countrys.add(new Country(13, "СССР"));
		countrys.add(new Country(25, "Австралия"));
		countrys.add(new Country(41, "Бельгия"));
		countrys.add(new Country(11, "Британия"));
		countrys.add(new Country(3, "Германия"));
		countrys.add(new Country(18, "(ФГР)Герм."));
		countrys.add(new Country(28, "Гонконг"));
		countrys.add(new Country(4, "Дания"));
		countrys.add(new Country(29, "Индия"));
		countrys.add(new Country(38, "Ирландия"));
		countrys.add(new Country(15, "Испания"));
		countrys.add(new Country(14, "Италия"));
		countrys.add(new Country(6, "Канада"));
		countrys.add(new Country(31, "Китай"));
		countrys.add(new Country(26, "Ю.Корея"));
		countrys.add(new Country(8, "Франция"));
		countrys.add(new Country(5, "Швеция"));
		countrys.add(new Country(9, "Япония"));
		return countrys;
	}
	
	
	
}
