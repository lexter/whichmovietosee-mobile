package com.duxgames.whichmovietosee.base;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class Film {

	public int _id;
	public Bitmap label;
	public Bitmap labelback;
	public String namerus;
	public String nameeng;
	public String genre;
	public String description;
	public String kinopoisk;
	public String imdb;
	public boolean inBookmark;
	
	public static String TABLE_BOOKMARK = "bookmark";
	
	public Film() {
		inBookmark = false;
	}
	
	public Film(Cursor cursor) {
		try {
			_id = cursor.getInt(cursor.getColumnIndex("_id"));
			namerus = 	cursor.getString(cursor.getColumnIndex("namerus"));
			nameeng = 	cursor.getString(cursor.getColumnIndex("nameeng"));
			label = BitmapFactory.decodeByteArray(cursor.getBlob(cursor.getColumnIndex("label")), 0, cursor.getBlob(cursor.getColumnIndex("label")).length);
			labelback = BitmapFactory.decodeByteArray(cursor.getBlob(cursor.getColumnIndex("labelback")), 0, cursor.getBlob(cursor.getColumnIndex("labelback")).length);
			JSONObject jsonObject = new JSONObject(cursor.getString(cursor.getColumnIndex("data")));
			genre = 	jsonObject.getString("genre");
			description = jsonObject.getString("description");
			kinopoisk = jsonObject.getString("kinopoisk");
			imdb = 		jsonObject.getString("imdb");
			inBookmark = true;
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void save(SQLiteDatabase db) {
		try {
			ContentValues insertValues = new ContentValues();
			if (_id!=0) insertValues.put("_id", _id);
			insertValues.put("namerus", namerus);
			insertValues.put("nameeng", nameeng);
			
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			label.compress(Bitmap.CompressFormat.PNG, 100, bos);
			byte[] blob = bos.toByteArray();
			insertValues.put("label", blob);
			
			ByteArrayOutputStream bos2 = new ByteArrayOutputStream();
			labelback.compress(Bitmap.CompressFormat.PNG, 100, bos2);
			byte[] blob2 = bos2.toByteArray();
			insertValues.put("labelback", blob2);
			
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("genre", genre);
			jsonObject.put("description", description);
			jsonObject.put("kinopoisk", kinopoisk);
			jsonObject.put("imdb", imdb);
			insertValues.put("data", jsonObject.toString());
			_id = (int) db.insert(TABLE_BOOKMARK, null, insertValues);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void updateCurrentBookmark(SQLiteDatabase db) {
		Cursor cursor = null;
		try {
			cursor = db.rawQuery("SELECT * FROM "+TABLE_BOOKMARK+" WHERE namerus LIKE '"+namerus+"' AND nameeng LIKE '"+nameeng+"'", null);
			if (cursor.moveToFirst()) {
				inBookmark = true;
				return;
			}
			inBookmark = false;
		} catch (Exception e) {
			e.printStackTrace();
			inBookmark = false;
		} finally {
			if (cursor!=null) cursor.close();
		}
	}
	
	public boolean remove(SQLiteDatabase db) {
		try {
			db.execSQL("DELETE FROM "+TABLE_BOOKMARK+" WHERE _id = "+_id);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean removeAll(SQLiteDatabase db,String table) {
		try {
			db.execSQL("DELETE FROM "+table+" WHERE 1");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static Film getFilmById(Context context, SQLiteDatabase db,int _id) {
		Cursor cursor = null;
		try {
			cursor = db.rawQuery("SELECT * FROM "+TABLE_BOOKMARK+" WHERE _id = "+_id, null);
			if (cursor.moveToFirst()) {
				Film film = new Film(cursor);
				return film;
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			if (cursor!=null) cursor.close();
		}
	}
	
	
	public static ArrayList<Film> getBookmarkFilms(Context context, SQLiteDatabase db) {
		ArrayList<Film> films = new ArrayList<Film>();
		Cursor cursor = null;
		try {
			cursor = db.rawQuery("SELECT * FROM "+TABLE_BOOKMARK+" ", null);
			while (cursor.moveToNext()) {
				Film film = new Film(cursor);
				films.add(film);
			}
			return films;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			if (cursor!=null) cursor.close();
		}
	}
	

	
}
