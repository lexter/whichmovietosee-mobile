package com.duxgames.whichmovietosee.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbOpenHelper extends SQLiteOpenHelper {
	
	private static final String 	TAG 				= "DbOpenHelper";
	private static final String		DATABASE_NAME		= "films.db";
	private static final int 		DATABASE_VERSION 	= 1;
	private static DbOpenHelper mInstance;
    private static SQLiteDatabase db;
	
	private static final String[]	CREATE_SCRIPT 		= new String[] {
												
		"CREATE TABLE IF NOT EXISTS	 bookmark  	(_id 			INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
												"label	 				blob,"+
												"labelback	 			blob,"+
												"namerus 				text not null unique,"+
												"nameeng 				text,"+
												"data 					text)",
		
    };

    
	public DbOpenHelper(Context context) {
	    super(context,DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static DbOpenHelper getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new DbOpenHelper(context);
        }
        return mInstance;
    }
   
   public synchronized SQLiteDatabase getDb() {
       if ((db == null) || (!db.isOpen())) {
           db = this.getWritableDatabase();
       }
       return db;
   }
   
   @Override
   public void close() {
       super.close();
       if (db != null) {
           db.close();
           db = null;
       }
       
       mInstance = null;
   }

	@Override
	public void onCreate(SQLiteDatabase db) {
		for (String sql_line : CREATE_SCRIPT) {
			try {
				db.execSQL(sql_line);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		
	}
	
	public static String DB_FILEPATH = "/data/data/com.duxgames.whichmovietosee/databases/"+DATABASE_NAME;


}
