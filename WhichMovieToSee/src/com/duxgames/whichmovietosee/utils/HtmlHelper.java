package com.duxgames.whichmovietosee.utils;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.json.JSONArray;

import android.text.Html;
import android.text.TextUtils;

public class HtmlHelper {

	TagNode rootNode;

	String filmName = "filmName";
	String kinopoiskRating = "rating  ratingGreenBG";
	String imdbRating = "imdb";
	String text = "syn";
	String pictureLink = "poster";
	String desc2 = "gray";
	
	public HtmlHelper(String htmlPage) throws Exception {

		//JSONArray object = new JSONArray(htmlPage);
		//htmlPage = object.getString(0);
		HtmlCleaner cleaner = new HtmlCleaner();
		rootNode = cleaner.clean(htmlPage);
	}

	ArrayList<TagNode> getElementsByName(TagNode tagnode,String name) {
		ArrayList<TagNode> nodes = new ArrayList<TagNode>();
		nodes.addAll(Arrays.asList(tagnode.getElementsByName(name, true)));
		return nodes;
	}
	
	public List<TagNode> getLinksByClass(String CSSClassname) {
		List<TagNode> linkList = new ArrayList<TagNode>();

		TagNode[] divElements = rootNode.getElementsHavingAttribute("class",true);

		for (int i = 0; divElements != null && i < divElements.length; i++) {
			String classType = divElements[i].getAttributeByName("class");
			if (classType != null && classType.equals(CSSClassname)) {
				linkList.add(divElements[i]);
			}
		}
		System.out.println(linkList.size());
		return linkList;
	}
	
	public String getNameRus() {
		if (rootNode!=null) {
			String name="";
			for (TagNode tagnode:getLinksByClass(filmName)) {
				TagNode[] tagnode2 = tagnode.getElementsByName("a", true);
				for (int i=0;i<tagnode2.length;i++)	name=tagnode2[i].getText().toString();
			}
			return Html.fromHtml((String) name).toString();
		} else {
			return "";
		}
	}

	public String getNameEng() {
		if (rootNode!=null) {
			String name="";
			for (TagNode tagnode:getLinksByClass(filmName)) {
				TagNode[] tagnode2 = tagnode.getElementsByName("span", true);
				for (int i=0;i<tagnode2.length;i++)	name=tagnode2[i].getText().toString();
			}
			return Html.fromHtml((String) name).toString();
		} else {
			return "";
		}
	}
	
	public String getDescription() {
		if (rootNode!=null) {
			String desc="";
			for (TagNode tagnode:getLinksByClass(text)) {
				desc=tagnode.getText().toString();
			}
			return Html.fromHtml((String) desc).toString();
		} else {
			return "";
		}
	}
	
	public String getGenre() {
		if (rootNode!=null) {
			String desc="";
			for (TagNode tagnode:getLinksByClass(desc2)) {
				desc+=tagnode.getText().toString()+"\n";
			}
			return Html.fromHtml((String) desc).toString();
		} else {
			return "";
		}
	}
	
	
	public String getRatingKino() {
		if (rootNode!=null) {
			String rating="";
			for (TagNode tagnode:getLinksByClass(kinopoiskRating)) {
				rating=tagnode.getText().toString();
			}
			
			rating = Html.fromHtml((String) rating).toString();
			rating = rating.replaceAll("  ", " ");
			while (rating.length()>0 && rating.charAt(0)==' ') {
				rating = rating.substring(1, rating.length());
			}
			for (int i=0;i<rating.length();i++) { 
				if (rating.charAt(i)==' ') {
					StringBuffer text = new StringBuffer(rating);
					rating = text.replace(i,rating.length(),"").toString();
					break;
				}
			}
			if (rating.equals("")) rating = "-";
			return rating;
		} else {
			return "-";
		}
	}
	
	public String getRatingImgb() { 
		if (rootNode!=null) {
			String rating="";
			for (TagNode tagnode:getLinksByClass(imdbRating)) {
				rating=tagnode.getText().toString();
			}
			rating = Html.fromHtml((String) rating).toString();
			rating = rating.replaceAll("  ", " ");
			rating = rating.replaceAll("IMDb: ", "");
			while (rating.length()>0 && rating.charAt(0)==' ') {
				rating = rating.substring(1, rating.length());
			}
			for (int i=0;i<rating.length();i++) { 
				if (rating.charAt(i)==' ') {
					StringBuffer text = new StringBuffer(rating);
					rating = text.replace(i,rating.length(),"").toString();
					break;
				}
			}
			if (rating.equals("")) rating = "-";
			return rating;
		} else {
			return "-";
		}
	}


	
	public String getPhotoLink() {
		if (rootNode!=null) {
			String link="";
			for (TagNode tagnode:getLinksByClass(pictureLink)) {
				System.out.println("a="+getElementsByName(tagnode,"a").size());
				for (TagNode nodeA:getElementsByName(tagnode,"a")) {
					System.out.println("img="+getElementsByName(tagnode,"img").size());
					for (TagNode nodeImg:getElementsByName(nodeA,"img")) {
						link = nodeImg.getAttributeByName("src").toString();
					}
				}
			}
			return "http://www.kinopoisk.ru"+Html.fromHtml((String) link).toString();
		} else {
			return "";
		}
	}
	
}