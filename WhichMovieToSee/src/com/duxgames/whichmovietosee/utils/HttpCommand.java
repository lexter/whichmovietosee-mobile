package com.duxgames.whichmovietosee.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;

public class HttpCommand {
	
	private HttpGet requestGet;
	private Header[] headers;
	private StatusLine statusline=null;
	private InputStream stream; 
	
	
	String URL = "http://kinopoisk.ru/view_random_film.php?item=false&count=1";
	
	public HttpCommand(Context context) {
		requestGet = new HttpGet(URL+getParams(context));
	}
	
	public String executeForString() throws Exception {
				
		String result = "";
		try {
			execute();
			BufferedReader reader = new BufferedReader(new InputStreamReader(stream,"windows-1251"));
			StringBuilder sb = new StringBuilder();
	        String line = null;
	        while ((line = reader.readLine()) != null) {
	      	  sb.append(line + System.getProperty("line.separator"));
	        }    
	        result = sb.toString();
		} catch (Exception e) {
			result = "";
			throw e;
		}
		return result;
	}

	String getParams(Context context) {
		String params="";
		try {
			JSONObject time = new JSONObject(Settings.getParam("time", context));
			params+="&min_years="+String.format("%tY", new Date(time.getLong("fromDate")));
			params+="&max_years="+String.format("%tY", new Date(time.getLong("toDate")));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			JSONArray genres = new JSONArray(Settings.getParam("genres", context));
			for (int i=0;i<genres.length();i++) {
				params+="&genre%5B%5D="+genres.getInt(i);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
			
		try {	
			JSONArray countrys = new JSONArray(Settings.getParam("countrys", context));
			for (int i=0;i<countrys.length();i++) {
				params+="&country%5B%5D="+countrys.getInt(i);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return params;
	}
	

	public HttpResponse execute() throws ClientProtocolException, IOException {
		HttpClient client = new DefaultHttpClient();
		client.getParams().setParameter("http.protocol.content-charset", HTTP.UTF_8);
		requestGet.addHeader("User-Agent","Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36");
		HttpResponse response = client.execute(requestGet);
		headers = response.getAllHeaders();
		stream = response.getEntity().getContent();
		statusline = response.getStatusLine();
		return response;
		
	}

	public String getHeader(String name) {
		for (Header header : headers) {
			if (header.getName().equals(name)) {
				return header.getValue();
			}			
		}
		return null;
	}
	

	public StatusLine getStatusline() {
		return statusline;
	}

	public InputStream getInputStream() {
		return stream;
	}

}
