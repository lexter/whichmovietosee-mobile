package com.duxgames.whichmovietosee.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

public class Settings {
	public static final int FIELD_WEIGHT = 15;
	public static final int FIELD_HEIGHT = 25;
	
	public static int SCREEN_WEIGHT;
	public static int SCREEN_HEIGHT;
	
	public static String SPEED;
	
	static SharedPreferences sPref;

	public static void setParam(String param, String value, Context context) {
		sPref = PreferenceManager.getDefaultSharedPreferences(context);
		Editor ed = sPref.edit();
		ed.putString(param, value);
		ed.commit();
	}

	public static String getParam(String param, Context context) {
		sPref = PreferenceManager.getDefaultSharedPreferences(context);
		return sPref.getString(param, "");
	}

}
